package com.daw.assignment4.service;

import com.daw.assignment4.dto.UserBuilder;
import com.daw.assignment4.dto.UserDTO;
import com.daw.assignment4.entity.ERole;
import com.daw.assignment4.entity.Role;
import com.daw.assignment4.entity.User;
import com.daw.assignment4.repository.RoleRepository;
import com.daw.assignment4.repository.UserRepository;
import javassist.NotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

@Service
@Slf4j
public class UserService {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final PasswordEncoder encoder;

    @Autowired
    public UserService(UserRepository userRepository, RoleRepository roleRepository,
                       PasswordEncoder encoder) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.encoder = encoder;
    }

    public UUID insert(UserDTO userDTO) {
        User user = UserBuilder.toEntity(userDTO);
        Set<Role> roles = new HashSet<>();
        String roleString = userDTO.getRole().toLowerCase();
        roles.add(roleRepository.findByName(ERole.fromString(roleString)));
        user.setRoles(roles);
        user.setPassword(encoder.encode(user.getPassword()));
        user = userRepository.save(user);
        return user.getId();
    }

    public UserDTO findByEmail(String email) throws NotFoundException {
        Optional<User> userOptional = userRepository.findByEmail(email);
        if (!userOptional.isPresent()) {
            log.error("Admin with email {} was not found in db.", email);
            throw new NotFoundException("User");
        }
        return UserBuilder.toDTO(userOptional.get());
    }
}
