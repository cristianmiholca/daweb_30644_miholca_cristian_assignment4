package com.daw.assignment4.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotBlank;
import java.util.UUID;

@Entity
@Table(	name = "jwtKeys",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = "user_id")
        })

public class JwtKey {
    @Id
    private UUID user_id;
    @NotBlank
    private String jwt;

    public JwtKey()
    {}

    public JwtKey(UUID user_id, String jwt)
    {
        this.user_id = user_id;
        this.jwt = jwt;
    }

    public UUID getUser_id() {
        return user_id;
    }

    public void setUser_id(UUID user_id) {
        this.user_id = user_id;
    }

    public String getJwt() {
        return jwt;
    }

    public void setJwt(String jwt) {
        this.jwt = jwt;
    }
}
