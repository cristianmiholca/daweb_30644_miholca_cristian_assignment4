package com.daw.assignment4.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.UUID;

@Entity
@Table(name = "appointment")
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class Appointment {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Type(type = "uuid-binary")
    private UUID id;

    @OneToOne
    @JoinColumn(name = "doctor_id", nullable = false)
    @NotNull
    private User doctor;

    @OneToOne
    @JoinColumn(name = "patient_id", nullable = false)
    @NotNull
    private User patient;

    @Column(name = "date")
    @NotNull
    private Date date;
}
