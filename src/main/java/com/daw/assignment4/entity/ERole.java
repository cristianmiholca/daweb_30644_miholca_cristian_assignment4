package com.daw.assignment4.entity;

public enum ERole {
    ROLE_PATIENT,
    ROLE_DOCTOR,
    ROLE_ADMIN,
    ROLE_GUEST;

    public static ERole fromString(String roleString) {
        switch (roleString.toLowerCase()) {
            case "doctor":
                return ROLE_DOCTOR;
            case "patient":
                return ROLE_PATIENT;
            case "admin":
                return ROLE_ADMIN;
            default:
                return ROLE_GUEST;
        }
    }

}
