package com.daw.assignment4.dto;

import com.daw.assignment4.entity.ERole;
import com.daw.assignment4.entity.Role;
import com.daw.assignment4.entity.User;

import java.util.HashSet;
import java.util.Set;

public class UserBuilder {

    public static User toEntity(UserDTO userDTO) {
        Set<Role> roles = new HashSet<>();
        if (userDTO.getRole() != null) {
            roles.add(new Role(ERole.fromString(userDTO.getRole())));
        }
        return new User(userDTO.getId(),
                getUsernameFromEmail(userDTO.getEmail()),
                userDTO.getEmail(),
                userDTO.getPassword(),
                roles);
    }

    public static UserDTO toDTO(User user) {
        String role = "";
        if (user.getRoles() != null && !user.getRoles().isEmpty()) {
            ERole eRole = user.getRoles().iterator().next().getName();
            role = getRoleFromEnum(eRole);
        }
        return new UserDTO(user.getId(),
                user.getEmail(),
                user.getPassword(),
                role);
    }

    private static String getUsernameFromEmail(String email) {
        String[] emailSplitted = email.split("@");
        return emailSplitted[0];
    }

    private static String getRoleFromEnum(ERole role) {
        switch (role) {
            case ROLE_ADMIN:
                return "admin";
            case ROLE_DOCTOR:
                return "doctor";
            case ROLE_PATIENT:
                return "patient";
            case ROLE_GUEST:
                return "guest";
            default:
                return "";
        }
    }

}
