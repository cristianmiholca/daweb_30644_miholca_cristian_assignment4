package com.daw.assignment4.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Getter
@AllArgsConstructor
public class UserDTO {

    private final UUID id;
    @NotNull
    private final String email;
    @NotNull
    private final String password;
    private final String role;
}
