package com.daw.assignment4.repository;

import com.daw.assignment4.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface UserRepository extends JpaRepository<User, UUID> {
    <T> Optional<T> findByUsername(String username);
    <T> Optional<T> findByEmail(String email);
}
