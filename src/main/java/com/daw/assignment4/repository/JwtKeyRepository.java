package com.daw.assignment4.repository;

import com.daw.assignment4.entity.JwtKey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JwtKeyRepository extends JpaRepository<JwtKey, Long> {

}
