package com.daw.assignment4.repository;

import com.daw.assignment4.entity.ERole;
import com.daw.assignment4.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    Role findByName(ERole name);
}
