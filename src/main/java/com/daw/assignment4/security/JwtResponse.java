package com.daw.assignment4.security;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.hateoas.RepresentationModel;

import java.util.List;
import java.util.UUID;

@Getter
@AllArgsConstructor
public class JwtResponse extends RepresentationModel<JwtResponse> {
    private final String jwt;
    private final UUID id;
    private final String username;
    private final List<String> role;
}

