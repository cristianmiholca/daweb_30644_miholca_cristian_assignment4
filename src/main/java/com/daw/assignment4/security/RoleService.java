package com.daw.assignment4.security;

import com.daw.assignment4.entity.ERole;
import com.daw.assignment4.entity.Role;
import com.daw.assignment4.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class RoleService {
    private final RoleRepository roleRepository;

    @Autowired
    public RoleService(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @PostConstruct
    public void initialize() {
        if (roleRepository.count() == 0) {
            roleRepository.deleteAll();
            roleRepository.save(new Role(ERole.ROLE_ADMIN));
            roleRepository.save(new Role(ERole.ROLE_PATIENT));
            roleRepository.save(new Role(ERole.ROLE_DOCTOR));
            roleRepository.save(new Role(ERole.ROLE_GUEST));
        }

    }
}

