package com.daw.assignment4.security;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.hateoas.RepresentationModel;

@Getter
@AllArgsConstructor
public class LoginRequest extends RepresentationModel<LoginRequest> {
    private final String email;
    private final String password;
}