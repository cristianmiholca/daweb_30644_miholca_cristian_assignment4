package com.daw.assignment4.security;

import com.daw.assignment4.entity.JwtKey;
import com.daw.assignment4.repository.JwtKeyRepository;
import com.daw.assignment4.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;


@Service
public class JwtKeyService {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);
    private final JwtKeyRepository jwtKeyRepository;

    @Autowired
    public JwtKeyService(JwtKeyRepository jwtKeyRepository) {
        this.jwtKeyRepository = jwtKeyRepository;
    }

    public UUID insert(JwtKey jwtKey) {
        jwtKey = jwtKeyRepository.save(jwtKey);
        return jwtKey.getUser_id();
    }

    public boolean verifyUser(String jwt, UUID user_id)
    {
        List<JwtKey> jwtKeyList = jwtKeyRepository.findAll();
        for(JwtKey j : jwtKeyList)
        {
            if(j.getUser_id() == user_id && j.getJwt().contains(jwt.replace("Bearer ", "")))
                return true;
        }
        return false;
    }

    public boolean verifyRole(String jwt, UUID role_id)
    {
        List<JwtKey> jwtKeyList = jwtKeyRepository.findAll();
        for(JwtKey j : jwtKeyList)
        {
            if(j.getJwt().contains(jwt.replace("Bearer ", "")))
                return true;
        }
        return false;
    }


}
