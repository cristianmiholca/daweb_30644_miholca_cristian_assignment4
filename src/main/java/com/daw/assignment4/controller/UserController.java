package com.daw.assignment4.controller;

import com.daw.assignment4.dto.UserDTO;
import com.daw.assignment4.entity.StringResponse;
import com.daw.assignment4.service.UserService;
import javassist.NotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;

@Slf4j
@RestController
@CrossOrigin
@RequestMapping(value = "/user")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping(value = "/create")
    public ResponseEntity<UUID> create(@Valid @RequestBody UserDTO userDTO) {
        UUID userId = userService.insert(userDTO);
        return new ResponseEntity<>(userId, HttpStatus.CREATED);
    }

    @GetMapping(value = "/getByEmail/{email}")
    public ResponseEntity<UUID> getByEmail(@PathVariable("email") String email) {
        try {
            UserDTO userDTO = userService.findByEmail(email);
            return new ResponseEntity<>(userDTO.getId(), HttpStatus.OK);
        } catch (NotFoundException e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/getRoleByEmail/{email}")
    public ResponseEntity<StringResponse> getRoleByEmail(@PathVariable("email") String email) {
        try {
            UserDTO userDTO = userService.findByEmail(email);
            return new ResponseEntity<>(new StringResponse(userDTO.getRole()), HttpStatus.OK);
        } catch (NotFoundException e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

}
